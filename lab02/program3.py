import os, sys

dirs = os.listdir("/bin/")
myfile = open('bincount.csv','w')

for i in range(1,30):

    count = 0;
    for file in dirs:
        if (len(file) == i):
            count+=1;
    myfile.write(str(i) + ', ' + str(count)+ '\n');
