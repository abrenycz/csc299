import csv
with open('data.csv','rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter = ' ', quotechar='|')
    
    # column totals
    one = 0;
    two = 0;
    three = 0;
    four = 0;

    for row in spamreader:
        one += int(row[0].strip(','));
        two += int(row[1].strip(','));
        three += int(row[2].strip(','));
        four += int(row[3].strip(','));

myfile = open('sums.csv','w');
myfile.write(str(one) + ', ' + str(two) + ', ' + str(three) + ', ' + str(four));
