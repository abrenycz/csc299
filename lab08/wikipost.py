import mechanize
import getpass

myUrl = getpass.getpass("url of website: ")
mySlug = getpass.getpass("input number for wiki slug: ")

from BeautifulSoup import BeautifulSoup

br = mechanize.Browser()
br.set_handle_robots(False)
br.set_handle_refresh(False)
br.addheaders = [('csc299','true')]

#extract the url information
response = br.open(myUrl)
html = br.response().read()
soup = BeautifulSoup(html)
myTitle = br.title()
response = br.open("http://mdp.cdm.depaul.edu/wiki")

#login
br.select_form(nr=0)
br.form['username'] = 'abrenycz'
br.form['password'] = '4815'
br.submit()

for link in br.links():
    if(link.text == "Create page \"index\""):
        response = br.follow_link(link)

#enter the slug
br.select_form(nr=0)
br.form['slug']='abrenycz-'+mySlug
br.submit()

#enter the title and body  # 

br.select_form(nr=0)
br.form['title']=myTitle
br.form['body']="Body text string goes here" # this never works when I set it to a string containing the original page's html content
#br.form['body']=str(html)
br.submit()

print "Attempting to create...: abrenycz-" + mySlug + " wiki."
print br.response().geturl()
