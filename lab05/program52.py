import urllib
import re
from BeautifulSoup import BeautifulSoup as Soup

url1="http://www.cdm.depaul.edu/odata/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20'A'%20and%20SubjectId%20eq'CSC'"

s = urllib.urlopen(url1)
data = s.read()

with open('CSC.xml', 'w') as myfile:
    myfile.write(data)

soup = Soup(data)
#temp = soup.findAll("m:properties")

temp = soup.find("m:properties").findAll(name = re.compile('^d\:\w+'))  

import simplejson
obj = {}
for x in temp:
    obj[x.name[2:]]=x.string
    simplejson.dump(obj, open('CSC.properties.1.json','w'))
copy = simplejson.load(open('obj.json','r'))
print obj == copy

#with open('CSC.properties.1.json','w') as myfile:
 #   myfile.write(str(copy))
