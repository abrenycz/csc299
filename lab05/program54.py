import urllib
import re
from BeautifulSoup import BeautifulSoup as Soup

url1="http://www.cdm.depaul.edu/odata/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20'A'%20and%20SubjectId%20eq'CSC'"

s = urllib.urlopen(url1)
data = s.read()

with open('CSC.xml', 'w') as myfile:
    myfile.write(data)

soup = Soup(data)
temp = soup.findAll("m:properties")

with open('CSC.properties.1.xml','w') as myfile:
    myfile.write(str(temp[0]))
