import csv

with open('expenses.csv','rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter = ',')
    
    a = {};
    counter = 0;

    for row in spamreader:
        if (counter > 0):
                a[row[0]] = a.get(row[0],0) + float(row[1][1:]);
               # print("adding..." + str(a[row[0]]));
        counter = counter + 1

myfile = open('totals.csv','w');
myfile.write('USER_ID, TOTAL_EXPENSE' + "\n");

for keys, values in a.items():
    myfile.write(keys + ', $' +  str(values) + "\n");
