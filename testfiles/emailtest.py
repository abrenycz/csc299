import re
import urllib

url = "http://www.depaul.edu/pages/contact-us.aspx"

page = urllib.urlopen(url)

r = re.compile('[\w\.\-]+@[\w\.\-]+')

emails = r.findall(page.read())
emails = list(set(emails))

for email in emails:
    print email
