Name: Andrew Brenycz
StudentID: 1276154
Email:abrenycz@gmail.com

kernal version: linux mdptest 2.6.32-64-server
machine hardware name: #128-ubuntu SMP
processor type: x86_64
operating system: GNU/LINUX

ls program location: /bin/ls
size of ls program: not sure
author: stallman and mackenzie
how to tell ls to list all hidden files: ls -a

/home - contains user files
/bin  - contains utility binary files for commands
/var  - contains files the system writes during operation
/etc  - contains many files such as scripts and user information

Crossword

ACROSS
7. Admin
8. rmdir
11. relative

DOWN
1. C
10. less
11. rm