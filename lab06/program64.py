import urllib
from BeautifulSoup import BeautifulSoup as Soup
import csv
import re
from operator import itemgetter

page = urllib.urlopen('http://www.superherodb.com/characters/').read()

soup = Soup(page)

letters = soup.find('ul',{'class':'char-ul'})
print letters
charsli= letters.findAll('li', attrs = {'class':'char-li'})
print charsli
count = 0

with open('ranking.csv','w') as myfile:
    writer = csv.writer(myfile)
    writer.writerow(['RANK', ' NAME', ' TOTAL_POWERS'])
    rankList = []

    for a in charsli:
#    print a.find('a').text
        if (True):
            newpage = a.find('a')['href']
            count = count + 1
            page2 = urllib.urlopen('http://www.superherodb.com/'+newpage)
            soup2 = Soup(page2)

            name = soup2.find('h1')
            
            stats = soup2.findAll('div', attrs = {'class': re.compile('gridbarvalue color*')})

            # replace empty stats with empy strings
            buffer = [0,0,0,0,0,0]
            bufcount = 0
            for x in stats:
                if (bufcount < 6):
                    buffer[bufcount] = x.text
                    bufcount = bufcount+1

            rankList.append([name.text, int(buffer[0])+int(buffer[1])+ int(buffer[2])+ int(buffer[3])+int( buffer[4])+int( buffer[5])])

    rank = 1
    rankList = sorted(rankList, key = itemgetter(1), reverse = True)
    for x in rankList:
        writer.writerow([str(rank), str(x[0]), str(x[1])])
        rank = rank + 1
