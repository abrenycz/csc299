import urllib
from BeautifulSoup import BeautifulSoup as Soup
import csv
import re

page = urllib.urlopen('http://www.superherodb.com/characters/').read()

soup = Soup(page)

charsli = soup.findAll('li', attrs = {'class':'char-li'})
count = 0

with open('superheroes.csv','w') as myfile:
    writer = csv.writer(myfile)
    writer.writerow([' NAME', ' INTELLIGENCE', ' STRENGTH', ' SPEED', ' DU\
RABILITY', ' POWER', ' COMBAT'])


    for a in charsli:
#    print a.find('a').text
        if (count < 10):
            newpage = a.find('a')['href']
            count = count + 1
            page2 = urllib.urlopen('http://www.superherodb.com/'+newpage)
            soup2 = Soup(page2)

            name = soup2.find('h1')
            
            stats = soup2.findAll('div', attrs = {'class': re.compile('gridbarvalue color*')})

            # replace empty stats with empy strings
            buffer = ["","","","","",""]
            bufcount = 0
            for x in stats:
                if (bufcount < 6):
                    buffer[bufcount] = x.text
                    bufcount = bufcount+1

            writer.writerow([name.text, buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]])
