import urllib
from BeautifulSoup import BeautifulSoup as Soup
import csv

page = urllib.urlopen('http://www.superherodb.com/characters/').read()

soup = Soup(page)

charsli = soup.findAll('li', attrs = {'class':'char-li'})
for a in charsli:
    print a.find('a').text

with open('names.csv','w') as myfile:
    myfile.write("NAME\n")
    for a in charsli:
        myfile.write(a.find('a').text)
        myfile.write("\n")
