import urllib
from BeautifulSoup import BeautifulSoup as Soup
import csv
import re

page = urllib.urlopen('http://www.superherodb.com/superman/10-791/').read()

soup = Soup(page)

stats = soup.findAll('div', attrs = {'class': re.compile('gridbarvalue color*')})
for x in stats:
    print x.text

with open('superman.csv','w') as myfile:
    writer = csv.writer(myfile)
    writer.writerow([' NAME', ' INTELLIGENCE', ' STRENGTH', ' SPEED', ' DURABILITY', ' POWER', ' COMBAT'])
    writer.writerow(["Superman", stats[0].text, stats[1].text, stats[2].text, stats[3].text, stats[4].text, stats[5].text])
