from fabric.api import *
import shutil
import urllib2

env.hosts = [ 'mdp.cdm.depaul.edu' ]
env.user = "csc299-1276154"
env.password = "antidead"
env.parallel = True

def deploy(filename):
    put(filename, '~/')
    run('unzip ~/%s' % filename)

def install_web2py():
    response = urllib2.urlopen('http://www.web2py.com/examples/static/web2py_src.zip')
    webcontent = response.read()
    with open("web2py_src.zip",'w') as f:
        f.write(webcontent)
    
    put('web2py_src.zip', '~/')
    run('unzip web2py_src.zip')

def update_fabfile():
    put('fabfile.py', '~/csc299/lab09')
